#!/bin/bash

git config user.name "Stefano Massarotti"
git config user.email "stefano.massarotti@nodeweaver.eu"
git config core.editor /usr/bin/mcedit
export EDITOR=/usr/bin/mcedit
export VISUAL=/usr/bin/mcedit